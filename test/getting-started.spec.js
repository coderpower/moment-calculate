var expect = require('expect.js');
var moment = require('moment');
var calculate = require('../sources/calculate.js');

suite('Adding and subtracting durations with moment.js', function(){

    test('Should return a string', function(){
        var result = calculate();
        expect(typeof result).to.be('string');
    });

    test('Should return the right date', function(){
        var date = new Date('2014/12/31');
        var expected = moment(date).add(1, 'day').format('l');
        var result = calculate();
        expect(result).to.be(expected);
    });

});